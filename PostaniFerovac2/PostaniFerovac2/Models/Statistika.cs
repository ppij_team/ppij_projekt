﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostaniFerovac2.Models
{
    public class Statistika
    {
        [Key]
        public int statistikaId { get; set; }
        public string postotakA { get; set; }

        public string postotakB { get; set; }

        public string postotakC { get; set; }

        public string postotakD { get; set; }

        public string odgovorenoRjesenje { get; set; }

        public string postotakRjesenosti { get; set; }

        public int prosjecnoVrijeme;

        public int trenutnoVrijeme;

        public bool tocan = false;
    }
}
