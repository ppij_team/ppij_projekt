﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PostaniFerovac2.Models
{
   // [Table("Zadatak")]
    public class Zadatak
    {

        public Zadatak() { }

        
        public int zadatakId { get; set; }

        public int tipZadatka { get; set; }

        public string tekstZadatka { get; set; }

        public string tekstARjesenje { get; set; }

        public string tekstBRjesenje { get; set; }

        public string tesktCRjesenje { get; set; }

        public string tesktDRjesenje { get; set; }

        public string tocnoRjesenje { get; set; }

        public string slika { get; set; }

        public int brOdgA { get; set; }

        public int brOdgB { get; set; }

        public int brOdgC { get; set; }

        public int brOdgD { get; set; }

        public int brTocnih { get; set; }

        public int brNetocnih { get; set; }

        public int cjelinaId { get; set; }
        public virtual Cjelina cjelina { get; set; }

    //    public int statistikaDMId { get; set; }
        public virtual StatistikaDM statistikaDM { get; set; }

        public int vrijemeSekunde { get; set; }

        public string napomena { get; set; }

    }
}