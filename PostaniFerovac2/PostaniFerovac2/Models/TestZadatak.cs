﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PostaniFerovac2.Models
{
    public class TestZadatak
    {

        [Key]
        public int testZadatakId { get; set; }

        public virtual Test test { get; set; }

        
        public int testId { get; set; }

       
        public virtual Zadatak zadatak { get; set; }

        
        public int zadatakId { get; set; }

        public string rjesenje { get; set; }

        public int? pomak { get; set; }

        public string odgA { get; set; }

        public string odgB { get; set; }

        public string odgC { get; set; }

        public string odgD { get; set; }

        public string tocnoRj { get; set; }

        public bool tocnost { get; set; }

    }
}