﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostaniFerovac2.Models
{
    public class Cjelina
    {
        public int cjelinaId { get; set; }

        public string naziv { get; set; }

        public string opis { get; set; }

        public int predmetId { get; set; }
        public virtual Predmet predmet { get; set; }

        public virtual List<Zadatak> zadaci { get; set; }
    }
}