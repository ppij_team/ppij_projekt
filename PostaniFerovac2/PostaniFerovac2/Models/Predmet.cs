﻿using System.Collections.Generic;

namespace PostaniFerovac2.Models
{
    public class Predmet
    {
        public int predmetId { get; set; }
        public string naziv { get; set; }

        public virtual List<Cjelina> cjeline { get; set; }
    }
}