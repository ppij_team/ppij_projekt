﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostaniFerovac2.Models
{
    public class Test
    {
        public int testId { get; set; }
        
        public string user { get; set; }
        public int brojPitanja { get; set; }
        public int tocnih { get; set; }
        public int vrijeme { get; set; }
        public DateTime? datum { get; set; }
        public List<TestZadatak> testZadaci { get; set; }
        public int bodovi { get; set; }
        public string username;
        public int predmetId;
    }
}