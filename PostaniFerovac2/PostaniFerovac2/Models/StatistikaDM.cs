﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PostaniFerovac2.Models
{
    public class StatistikaDM
    {
        [ForeignKey("zadatak")]
        public int statistikaDMId { get; set; }
        //public int zadatakId { get; set; }

        public int brOdgA { get; set; }

        public int brOdgB { get; set; }

        public int brOdgC { get; set; }

        public int brOdgD { get; set; }

        public string tocanOdgovor { get; set; }

        public int postotakRjesenosti { get; set; }

       // public int zadatakId { get; set; }

      //  [ForeignKey("zadatakId")]
        public virtual Zadatak zadatak { get; set; }
    }
}