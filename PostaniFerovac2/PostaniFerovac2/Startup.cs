﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PostaniFerovac2.Startup))]
namespace PostaniFerovac2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
