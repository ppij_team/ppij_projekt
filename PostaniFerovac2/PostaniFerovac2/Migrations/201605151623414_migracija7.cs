namespace PostaniFerovac2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracija7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Zadataks", "vrijemeSekunde", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Zadataks", "vrijemeSekunde");
        }
    }
}
