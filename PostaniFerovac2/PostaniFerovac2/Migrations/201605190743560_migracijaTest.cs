namespace PostaniFerovac2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracijaTest : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.TestZadataks", "pomak");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TestZadataks", "pomak", c => c.Int());
        }
    }
}
