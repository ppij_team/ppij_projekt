namespace PostaniFerovac2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class daradipomak : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TestZadataks", "pomak", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TestZadataks", "pomak");
        }
    }
}
