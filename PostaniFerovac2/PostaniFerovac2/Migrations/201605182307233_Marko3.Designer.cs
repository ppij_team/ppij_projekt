// <auto-generated />
namespace PostaniFerovac2.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Marko3 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Marko3));
        
        string IMigrationMetadata.Id
        {
            get { return "201605182307233_Marko3"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
