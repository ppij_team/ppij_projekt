namespace PostaniFerovac2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig8 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Statistikas",
                c => new
                    {
                        statistikaId = c.Int(nullable: false, identity: true),
                        postotakA = c.String(),
                        postotakB = c.String(),
                        postotakC = c.String(),
                        postotakD = c.String(),
                        odgovorenoRjesenje = c.String(),
                        postotakRjesenosti = c.String(),
                    })
                .PrimaryKey(t => t.statistikaId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Statistikas");
        }
    }
}
