namespace PostaniFerovac2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tests", "user", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tests", "user");
        }
    }
}
