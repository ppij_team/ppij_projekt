namespace PostaniFerovac2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Marko2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Tests", "datum", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tests", "datum", c => c.DateTime(nullable: false));
        }
    }
}
