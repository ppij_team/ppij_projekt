namespace PostaniFerovac2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Marko4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Zadataks", "napomena", c => c.String());
            AddColumn("dbo.TestZadataks", "odgA", c => c.String());
            AddColumn("dbo.TestZadataks", "odgB", c => c.String());
            AddColumn("dbo.TestZadataks", "odgC", c => c.String());
            AddColumn("dbo.TestZadataks", "odgD", c => c.String());
            AddColumn("dbo.TestZadataks", "tocnoRj", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TestZadataks", "tocnoRj");
            DropColumn("dbo.TestZadataks", "odgD");
            DropColumn("dbo.TestZadataks", "odgC");
            DropColumn("dbo.TestZadataks", "odgB");
            DropColumn("dbo.TestZadataks", "odgA");
            DropColumn("dbo.Zadataks", "napomena");
        }
    }
}
