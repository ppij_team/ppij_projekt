namespace PostaniFerovac2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Marko3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tests", "bodovi", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tests", "bodovi");
        }
    }
}
