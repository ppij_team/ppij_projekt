namespace PostaniFerovac2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Marko1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tests", "vrijeme", c => c.Int(nullable: false));
            AddColumn("dbo.Tests", "datum", c => c.DateTime(nullable: false));
            DropColumn("dbo.Tests", "userId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tests", "userId", c => c.Int(nullable: false));
            DropColumn("dbo.Tests", "datum");
            DropColumn("dbo.Tests", "vrijeme");
        }
    }
}
