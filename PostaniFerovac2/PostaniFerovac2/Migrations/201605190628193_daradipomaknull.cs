namespace PostaniFerovac2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class daradipomaknull : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TestZadataks", "pomak", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TestZadataks", "pomak", c => c.Int(nullable: false));
        }
    }
}
