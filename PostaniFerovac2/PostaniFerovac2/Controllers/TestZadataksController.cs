﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PostaniFerovac2.Models;

namespace PostaniFerovac2.Controllers
{
    public class TestZadataksController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TestZadataks
        public ActionResult Index()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            var testZadataks = db.TestZadataks.Include(t => t.test).Include(t => t.zadatak);
            return View(testZadataks.ToList());
        }

        // GET: TestZadataks/Details/5
        public ActionResult Details(int? id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TestZadatak testZadatak = db.TestZadataks.Find(id);
            if (testZadatak == null)
            {
                return HttpNotFound();
            }
            return View(testZadatak);
        }

        // GET: TestZadataks/Create
        public ActionResult Create()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.testId = new SelectList(db.Tests, "testId", "testId");
            ViewBag.zadatakId = new SelectList(db.zadaci, "zadatakId", "tekstZadatka");
            return View();
        }

        // POST: TestZadataks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "testZadatakId,testId,zadatakId,rjesenje")] TestZadatak testZadatak)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (ModelState.IsValid)
            {
                db.TestZadataks.Add(testZadatak);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.testId = new SelectList(db.Tests, "testId", "testId", testZadatak.testId);
            ViewBag.zadatakId = new SelectList(db.zadaci, "zadatakId", "tekstZadatka", testZadatak.zadatakId);
            return View(testZadatak);
        }

        // GET: TestZadataks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TestZadatak testZadatak = db.TestZadataks.Find(id);
            if (testZadatak == null)
            {
                return HttpNotFound();
            }
            ViewBag.testId = new SelectList(db.Tests, "testId", "testId", testZadatak.testId);
            ViewBag.zadatakId = new SelectList(db.zadaci, "zadatakId", "tekstZadatka", testZadatak.zadatakId);
            return View(testZadatak);
        }

        // POST: TestZadataks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "testZadatakId,testId,zadatakId,rjesenje")] TestZadatak testZadatak)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (ModelState.IsValid)
            {
                db.Entry(testZadatak).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.testId = new SelectList(db.Tests, "testId", "testId", testZadatak.testId);
            ViewBag.zadatakId = new SelectList(db.zadaci, "zadatakId", "tekstZadatka", testZadatak.zadatakId);
            return View(testZadatak);
        }

        // GET: TestZadataks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TestZadatak testZadatak = db.TestZadataks.Find(id);
            if (testZadatak == null)
            {
                return HttpNotFound();
            }
            return View(testZadatak);
        }

        // POST: TestZadataks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            TestZadatak testZadatak = db.TestZadataks.Find(id);
            db.TestZadataks.Remove(testZadatak);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
