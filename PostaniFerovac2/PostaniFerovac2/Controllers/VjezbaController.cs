﻿using PostaniFerovac2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PostaniFerovac2.Controllers
{
    public class VjezbaController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext();
        private Zadatak trenutniZadatak;
        private Random rand = new Random();

        // GET: Vjezba/5
        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Predmet predmet = db.predmeti.Find(id);
            if (predmet == null)
            {
                return HttpNotFound();
            }
            
            return View(db.cjeline.Where(c => c.predmetId == predmet.predmetId));
        }

        [HttpPost]
        public ActionResult OdabirCjeline()
        {
            List<Zadatak> zadaci = db.zadaci.ToList();
            int brZadatka = rand.Next(zadaci.Count - 1);
            trenutniZadatak = zadaci[brZadatka];
            return View();
        }

        
    }
}