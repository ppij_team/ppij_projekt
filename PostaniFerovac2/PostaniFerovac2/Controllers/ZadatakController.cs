﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PostaniFerovac2.Models;

namespace PostaniFerovac2.Controllers
{
    public class ZadatakController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Zadatak
        public ActionResult Index()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            var zadaci = db.zadaci.Include(z => z.cjelina).Include(z => z.statistikaDM);
            return View(zadaci.ToList());
        }

        // GET: Zadatak/Details/5
        public ActionResult Details(int? id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zadatak zadatak = db.zadaci.Find(id);
            if (zadatak == null)
            {
                return HttpNotFound();
            }
            return View(zadatak);
        }

        // GET: Zadatak/Create
        public ActionResult Create()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.cjelinaId = new SelectList(db.cjeline, "cjelinaId", "naziv");
            ViewBag.zadatakId = new SelectList(db.statistike, "statistikaDMId", "tocanOdgovor");
            return View();
        }

        // POST: Zadatak/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "zadatakId,tipZadatka,tekstZadatka,tekstARjesenje,tekstBRjesenje,tesktCRjesenje,tesktDRjesenje,tocnoRjesenje,slika,brOdgA,brOdgB,brOdgC,brOdgD,brTocnih,brNetocnih,cjelinaId,napomena")] Zadatak zadatak)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (ModelState.IsValid)
            {
                db.zadaci.Add(zadatak);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.cjelinaId = new SelectList(db.cjeline, "cjelinaId", "naziv", zadatak.cjelinaId);
            ViewBag.zadatakId = new SelectList(db.statistike, "statistikaDMId", "tocanOdgovor", zadatak.zadatakId);
            return View(zadatak);
        }

        // GET: Zadatak/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zadatak zadatak = db.zadaci.Find(id);
            if (zadatak == null)
            {
                return HttpNotFound();
            }
            ViewBag.cjelinaId = new SelectList(db.cjeline, "cjelinaId", "naziv", zadatak.cjelinaId);
            ViewBag.zadatakId = new SelectList(db.statistike, "statistikaDMId", "tocanOdgovor", zadatak.zadatakId);
            return View(zadatak);
        }

        // POST: Zadatak/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "zadatakId,tipZadatka,tekstZadatka,tekstARjesenje,tekstBRjesenje,tesktCRjesenje,tesktDRjesenje,tocnoRjesenje,slika,brOdgA,brOdgB,brOdgC,brOdgD,brTocnih,brNetocnih,cjelinaId,napomena")] Zadatak zadatak)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (ModelState.IsValid)
            {
                db.Entry(zadatak).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.cjelinaId = new SelectList(db.cjeline, "cjelinaId", "naziv", zadatak.cjelinaId);
            ViewBag.zadatakId = new SelectList(db.statistike, "statistikaDMId", "tocanOdgovor", zadatak.zadatakId);
            return View(zadatak);
        }

        // GET: Zadatak/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zadatak zadatak = db.zadaci.Find(id);
            if (zadatak == null)
            {
                return HttpNotFound();
            }
            return View(zadatak);
        }

        // POST: Zadatak/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            Zadatak zadatak = db.zadaci.Find(id);
            db.zadaci.Remove(zadatak);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
          
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // POST: Zadatak/Test
        // dummy method
        public ActionResult Test()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            return null;
        }
    }
}
