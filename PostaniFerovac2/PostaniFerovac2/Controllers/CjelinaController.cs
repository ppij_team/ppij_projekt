﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PostaniFerovac2.Models;

namespace PostaniFerovac2.Controllers
{
    public class CjelinaController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Cjelina
        public ActionResult Index()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            var cjeline = db.cjeline.Include(c => c.predmet);
            return View(cjeline.ToList());
        }

        // GET: Cjelina/Details/5
        public ActionResult Details(int? id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cjelina cjelina = db.cjeline.Find(id);
            if (cjelina == null)
            {
                return HttpNotFound();
            }
            return View(cjelina);
        }

        // GET: Cjelina/Create
        public ActionResult Create()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.predmetId = new SelectList(db.predmeti, "predmetId", "naziv");
            return View();
        }

        // POST: Cjelina/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "cjelinaId,naziv,opis,predmetId")] Cjelina cjelina)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (ModelState.IsValid)
            {
                db.cjeline.Add(cjelina);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.predmetId = new SelectList(db.predmeti, "predmetId", "naziv", cjelina.predmetId);
            return View(cjelina);
        }

        // GET: Cjelina/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cjelina cjelina = db.cjeline.Find(id);
            if (cjelina == null)
            {
                return HttpNotFound();
            }
            ViewBag.predmetId = new SelectList(db.predmeti, "predmetId", "naziv", cjelina.predmetId);
            return View(cjelina);
        }

        // POST: Cjelina/Edit/5
        // To protect from overpoxcvbbvcxzzx xcvbnsting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "cjelinaId,naziv,opis,predmetId")] Cjelina cjelina)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (ModelState.IsValid)
            {
                db.Entry(cjelina).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.predmetId = new SelectList(db.predmeti, "predmetId", "naziv", cjelina.predmetId);
            return View(cjelina);
        }

        // GET: Cjelina/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cjelina cjelina = db.cjeline.Find(id);
            if (cjelina == null)
            {
                return HttpNotFound();
            }
            return View(cjelina);
        }

        // POST: Cjelina/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            Cjelina cjelina = db.cjeline.Find(id);
            db.cjeline.Remove(cjelina);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
           
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
