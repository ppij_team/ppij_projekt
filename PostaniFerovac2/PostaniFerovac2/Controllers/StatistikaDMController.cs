﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PostaniFerovac2.Models;

namespace PostaniFerovac2.Controllers
{
    public class StatistikaDMController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: StatistikaDM
        public ActionResult Index()
        {
            var statistike = db.statistike.Include(s => s.zadatak);
            return View(statistike.ToList());
        }

        // GET: StatistikaDM/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StatistikaDM statistikaDM = db.statistike.Find(id);
            if (statistikaDM == null)
            {
                return HttpNotFound();
            }
            return View(statistikaDM);
        }

        // GET: StatistikaDM/Create
        public ActionResult Create()
        {
            ViewBag.statistikaDMId = new SelectList(db.zadaci, "zadatakId", "tekstZadatka");
            return View();
        }

        // POST: StatistikaDM/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "statistikaDMId,brOdgA,brOdgB,brOdgC,brOdgD,tocanOdgovor,postotakRjesenosti")] StatistikaDM statistikaDM)
        {
            if (ModelState.IsValid)
            {
                db.statistike.Add(statistikaDM);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.statistikaDMId = new SelectList(db.zadaci, "zadatakId", "tekstZadatka", statistikaDM.statistikaDMId);
            return View(statistikaDM);
        }

        // GET: StatistikaDM/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StatistikaDM statistikaDM = db.statistike.Find(id);
            if (statistikaDM == null)
            {
                return HttpNotFound();
            }
            ViewBag.statistikaDMId = new SelectList(db.zadaci, "zadatakId", "tekstZadatka", statistikaDM.statistikaDMId);
            return View(statistikaDM);
        }

        // POST: StatistikaDM/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "statistikaDMId,brOdgA,brOdgB,brOdgC,brOdgD,tocanOdgovor,postotakRjesenosti")] StatistikaDM statistikaDM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(statistikaDM).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.statistikaDMId = new SelectList(db.zadaci, "zadatakId", "tekstZadatka", statistikaDM.statistikaDMId);
            return View(statistikaDM);
        }

        // GET: StatistikaDM/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StatistikaDM statistikaDM = db.statistike.Find(id);
            if (statistikaDM == null)
            {
                return HttpNotFound();
            }
            return View(statistikaDM);
        }

        // POST: StatistikaDM/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            StatistikaDM statistikaDM = db.statistike.Find(id);
            db.statistike.Remove(statistikaDM);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
