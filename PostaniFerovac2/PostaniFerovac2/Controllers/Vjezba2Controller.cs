﻿using PostaniFerovac2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PostaniFerovac2.Controllers
{
    public class Vjezba2Controller : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext();
        private static Zadatak trenutniZadatak;
        private Random rand = new Random();
        private static DateTime trenutacnoVrijeme;
        static bool refresh = true;
        // key: pravi zadatak
        private static Dictionary<int, int> mapaPoretka;
        private static TestsController test = new TestsController();


        // GET: Vjezba2/5
        public ActionResult Index(int? id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Predmet predmet = db.predmeti.Find(id);
            if (predmet == null)
            {
                return HttpNotFound();
            }
            string naziv = predmet.naziv.Substring(0, predmet.naziv.Length - 1) + "e";
            IEnumerable<Cjelina> cjeline = db.cjeline.Where(c => c.predmetId == predmet.predmetId);
            Tuple<IEnumerable<Cjelina>, Zadatak, int, string> tuple = new 
                Tuple<IEnumerable<Cjelina>, Zadatak, int, string>(cjeline, null, -1, naziv);

            return View(tuple);
        }

        [HttpPost]
        public ActionResult Index(int cjelinaId)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            Cjelina cjelina = db.cjeline.Find(cjelinaId);
            IEnumerable<Cjelina> cjeline = db.cjeline.Where(c => c.predmetId == cjelina.predmetId);
            List<Zadatak> zadaci = db.zadaci.Where(z => z.cjelinaId == cjelinaId).ToList();
            Zadatak zadatak = null;
            if (zadaci.Count == 1)
            {
                zadatak = zadaci[rand.Next(zadaci.Count)];
                
            } else if (zadaci.Count != 0)
            {
                while (true)
                {
                    zadatak = zadaci[rand.Next(zadaci.Count)];
                  //  zadatak = zadaci.First(z => z.tipZadatka == 5);
                    if (!(trenutniZadatak != null && trenutniZadatak.zadatakId == zadatak.zadatakId))
                    {
                        break;
                    }
                }
            }
            if (zadatak.tipZadatka == 2 || zadatak.tipZadatka == 4 || zadatak.tipZadatka == 5)
            {
                trenutniZadatak = zadatak;
            } else
            {
                trenutniZadatak = stvoriZadatak(zadatak);
            }
            
            System.Diagnostics.Debug.WriteLine(zadatak);
            string naziv = cjelina.predmet.naziv.Substring(0, cjelina.predmet.naziv.Length - 1) + "e";
            Tuple<IEnumerable<Cjelina>, Zadatak, int, string> tuple = 
                new Tuple<IEnumerable<Cjelina>, Zadatak, int, string>(cjeline, trenutniZadatak, cjelinaId, naziv);
            trenutacnoVrijeme = DateTime.Now;
            refresh = false;
            return View(tuple);
        }

        private Zadatak stvoriZadatak(Zadatak zadatak)
        {
           
            Zadatak novi = new Zadatak();
            novi.brNetocnih = zadatak.brNetocnih;
            novi.brTocnih = zadatak.brTocnih;
            novi.cjelina = zadatak.cjelina;
            novi.cjelinaId = zadatak.cjelinaId;
            novi.slika = zadatak.slika;
            novi.tekstZadatka = zadatak.tekstZadatka;
            novi.tipZadatka = zadatak.tipZadatka;
            novi.zadatakId = zadatak.zadatakId;
            novi.vrijemeSekunde = zadatak.vrijemeSekunde;
            novi.napomena = zadatak.napomena;

            mapaPoretka = new Dictionary<int, int>();
            List<int> provjera = new List<int>();
            Dictionary<int, string> mapaOdgovor = new Dictionary<int, string>()
            {
                {0, zadatak.tekstARjesenje },
                {1, zadatak.tekstBRjesenje },
                {2, zadatak.tesktCRjesenje },
                {3, zadatak.tesktDRjesenje }
            };

            Dictionary<int, int> mapaBrOdgovora = new Dictionary<int, int>()
            {
                {0, zadatak.brOdgA },
                {1, zadatak.brOdgB },
                {2, zadatak.brOdgC },
                {3, zadatak.brOdgD }
            };
            for (int i=0; i<4; i++)
            {
                int j = 0;
                while (true)
                {
                    j = rand.Next(4);
                    if (!(provjera.Contains(j)))
                    {
                        provjera.Add(j);
                        break;
                    }
                }
                mapaPoretka.Add(i, j);

                switch(j)
                {
                    case 0 :
                        novi.tekstARjesenje = mapaOdgovor[i];
                        novi.brOdgA = mapaBrOdgovora[i];
                        
                        break;
                    case 1:
                        novi.tekstBRjesenje = mapaOdgovor[i];
                        novi.brOdgB = mapaBrOdgovora[i];
                        break;
                    case 2:
                        novi.tesktCRjesenje = mapaOdgovor[i];
                        novi.brOdgC = mapaBrOdgovora[i];
                        break;
                    case 3:
                        novi.tesktDRjesenje = mapaOdgovor[i];
                        novi.brOdgD = mapaBrOdgovora[i];
                        break;
                }

            }

            Dictionary<int, string> noviMapaOdgovor = new Dictionary<int, string>()
            {
                {0, "A" },
                {1, "B" },
                {2, "C" },
                {3, "D" }
            };

            if (zadatak.tocnoRjesenje.Equals("A"))
            {
                novi.tocnoRjesenje = noviMapaOdgovor[mapaPoretka[0]];
            } else if (zadatak.tocnoRjesenje.Equals("B"))
            {
                novi.tocnoRjesenje = noviMapaOdgovor[mapaPoretka[1]];
            } else if (zadatak.tocnoRjesenje.Equals("C"))
            {
                novi.tocnoRjesenje = noviMapaOdgovor[mapaPoretka[2]];
            } else if (zadatak.tocnoRjesenje.Equals("D"))
            {
                novi.tocnoRjesenje = noviMapaOdgovor[mapaPoretka[3]];
            }
            return novi;
        }

        public ActionResult Statistika()
        {
            return RedirectToAction("Index", "Home");
        }


        [HttpPost]
        public ActionResult Statistika(string rjesenje)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (refresh)
            {
                refresh = false;
                int id = trenutniZadatak.cjelina.cjelinaId;
                Cjelina cjelina = db.cjeline.First(c => c.cjelinaId == id);
                return RedirectToAction("Index", "Vjezba2", new { id = cjelina.predmetId, cjelinaId = id });
            }
            if (rjesenje == null || trenutniZadatak == null)
            {
                return HttpNotFound();
            }
            

            Zadatak zadatak = db.zadaci.Find(trenutniZadatak.zadatakId);

            double postotakA = 0;
            double postotakB = 0;
            double postotakC = 0;
            double postotakD = 0;
            double brOdgovora = 0;

            Statistika statistika = new Statistika();
            double rjesenost = 0;

            if (zadatak.tipZadatka == 1 || zadatak.tipZadatka == 3 || zadatak.tipZadatka == 4)
            {
                brOdgovora = zadatak.brOdgA + zadatak.brOdgB + zadatak.brOdgC + zadatak.brOdgD + 1;
                

                if (rjesenje.Equals("A"))
                {
                    trenutniZadatak.brOdgA += 1;
                    if (zadatak.tipZadatka == 4)
                    {
                        zadatak.brOdgA += 1;
                    } else
                    {
                        int index = mapaPoretka.Values.ToList().IndexOf(0);
                        dodajBroj(zadatak, index);
                        
                    }
                    
                }
                else if (rjesenje.Equals("B"))
                {
                    trenutniZadatak.brOdgB += 1;
                    if (zadatak.tipZadatka == 4)
                    {
                        zadatak.brOdgB += 1;
                    } else
                    {
                        int index = mapaPoretka.Values.ToList().IndexOf(1);
                        dodajBroj(zadatak, index);
                    }
                }
                else if (rjesenje.Equals("C"))
                {
                    trenutniZadatak.brOdgC += 1;
                    if (zadatak.tipZadatka == 4)
                    {
                        zadatak.brOdgC += 1;
                    } else
                    {
                        int index = mapaPoretka.Values.ToList().IndexOf(2);
                        dodajBroj(zadatak, index);
                    }
                }
                else if (rjesenje.Equals("D"))
                {
                    trenutniZadatak.brOdgD += 1;
                    if (zadatak.tipZadatka == 4)
                    {
                        zadatak.brOdgD += 1;
                    } else
                    {
                        int index = mapaPoretka.Values.ToList().IndexOf(3);
                        dodajBroj(zadatak, index);
                    }
                }
                
                if (trenutniZadatak.tocnoRjesenje.Equals(rjesenje))
                {
                    zadatak.brTocnih += 1;
                    statistika.tocan = true;
                } else
                {
                    zadatak.brNetocnih += 1;
                    statistika.tocan = false;
                }

                postotakA = ((double)trenutniZadatak.brOdgA / brOdgovora) * 100;
                postotakB = ((double)trenutniZadatak.brOdgB / brOdgovora) * 100;
                postotakC = ((double)trenutniZadatak.brOdgC / brOdgovora) * 100;
                postotakD = ((double)trenutniZadatak.brOdgD / brOdgovora) * 100;

                if (zadatak.tocnoRjesenje.Equals("A"))
                {
                    rjesenost = (double)zadatak.brOdgA / brOdgovora * 100;
                } else if (zadatak.tocnoRjesenje.Equals("B"))
                {
                    rjesenost = (double)zadatak.brOdgB / brOdgovora * 100;
                } else if (zadatak.tocnoRjesenje.Equals("C"))
                {
                    rjesenost = (double)zadatak.brOdgC / brOdgovora * 100;
                } else if (zadatak.tocnoRjesenje.Equals("D"))
                {
                    rjesenost = (double)zadatak.brOdgD / brOdgovora * 100;
                }
                


            }
            else if (zadatak.tipZadatka == 2 || zadatak.tipZadatka == 5)
            {
                
                string preoblikovaoRjesenje = rjesenje;
                preoblikovaoRjesenje = test.preoblikuj(preoblikovaoRjesenje);
                if (trenutniZadatak.tocnoRjesenje.Trim().Equals(rjesenje.Trim()))
                {
                    zadatak.brTocnih += 1;
                    statistika.tocan = true;
                }
                else if (test.usporediPogresku(preoblikovaoRjesenje, trenutniZadatak.tocnoRjesenje))
                {   // pogreska je dovoljno malena, tocno je!
                    zadatak.brTocnih += 1;
                    statistika.tocan = true;
                } else
                {
                    zadatak.brNetocnih += 1;
                    statistika.tocan = false;
                }

                //if (trenutniZadatak.tocnoRjesenje.Trim().Equals(rjesenje.Trim()))
                //{
                //    zadatak.brTocnih += 1;
                //    statistika.tocan = true;
                //} else
                //{
                //    zadatak.brNetocnih += 1;
                //    statistika.tocan = false;
                //}

                brOdgovora = zadatak.brNetocnih + zadatak.brTocnih;
                rjesenost = (double)zadatak.brTocnih / brOdgovora * 100;
            }
            

            IEnumerable<Cjelina> cjeline = db.cjeline.Where(c => c.predmetId == zadatak.cjelina.predmetId);
            string naziv = zadatak.cjelina.predmet.naziv.Substring(0, zadatak.cjelina.predmet.naziv.Length - 1) + "e";
            

            
            statistika.postotakA = string.Format("{0:0.00}", postotakA);
            statistika.postotakB = string.Format("{0:0.00}", postotakB);
            statistika.postotakC = string.Format("{0:0.00}", postotakC);
            statistika.postotakD = string.Format("{0:0.00}", postotakD);
            statistika.odgovorenoRjesenje = rjesenje;
            if (brOdgovora != 0)
            {
                brOdgovora = zadatak.brNetocnih + zadatak.brTocnih;
                statistika.postotakRjesenosti = string.Format("{0:0.00}", rjesenost);

            } else
            {
                statistika.postotakA = "0";
                statistika.postotakB = "0";
                statistika.postotakC = "0";
                statistika.postotakD = "0";
                statistika.postotakRjesenosti = "0";
            }
            
            if (trenutacnoVrijeme != null)
            {
                double diffInSeconds = (trenutacnoVrijeme - DateTime.Now).TotalSeconds;
                statistika.trenutnoVrijeme = -(int)diffInSeconds;
                
                zadatak.vrijemeSekunde -= (int)diffInSeconds;
                statistika.prosjecnoVrijeme = (int) ((double)zadatak.vrijemeSekunde / brOdgovora);
            }

            db.SaveChanges();

            System.Diagnostics.Debug.WriteLine(rjesenje);
            System.Diagnostics.Debug.WriteLine(trenutniZadatak.tocnoRjesenje);

            Tuple<IEnumerable<Cjelina>, Zadatak, int, Statistika> tuple =
                new Tuple<IEnumerable<Cjelina>, Zadatak, int, Statistika>(cjeline, trenutniZadatak, zadatak.cjelina.cjelinaId, statistika);
            refresh = true;
            return View(tuple);
        }

        private void dodajBroj(Zadatak zadatak, int index)
        {
            switch (index)
            {
                case 0:
                    zadatak.brOdgA += 1;
                    break;
                case 1:
                    zadatak.brOdgB += 1;
                    break;
                case 2:
                    zadatak.brOdgC += 1;
                    break;
                case 3:
                    zadatak.brOdgD += 1;
                    break;
            }
        }

    }
}