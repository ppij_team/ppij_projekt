﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using PostaniFerovac2.Models;
using Microsoft.AspNet.Identity;

namespace PostaniFerovac2.Controllers
{
    [HandleError]
    public class TestsController : Controller
    {
        Test test = new Test();
        private static Int32 konstBrojPitanja = 10;    // 10 po dogovoru
        private Int32 brojPrikazanih = 20;
        Random rnd = new Random();
        private ApplicationDbContext db = new ApplicationDbContext();
        

        // GET: Tests
        public ActionResult Index()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            string identity = User.Identity.GetUserId();
            List<Test> tests = db.Tests.Where( t => t.user == identity ).ToList();
            List<String> subjects = new List<string>();
            foreach (Test t in tests)
            {
                // generirati listu ispita
                subjects.Add(dohvatiNazivPredmeta(t));
            }
            Tuple<IEnumerable<Test>, IEnumerable<string>> tuple = new Tuple<IEnumerable<Test>, IEnumerable<string>>(tests,subjects);
            return View(tuple);
        }

        // GET: Tests/Details/5
        public ActionResult Details(int? id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            int uId;    // ovo bude korisnicki id
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                uId = (int)id;
            }
            Test dummy = db.Tests.Where(o => o.testId == uId).First();
            /*dummy.user = User.Identity.GetUserId();
            dummy.testId = uId;*/

            List<TestZadatak> testZadaci = db.TestZadataks.Where(o => o.testId == id).ToList();
            /*if (test == null)
            {
                return HttpNotFound();
            }*/
            List<Zadatak> zadaci = new List<Zadatak>();
            foreach (TestZadatak tz in testZadaci)
            {
                zadaci.Add(db.zadaci.Where(o => o.zadatakId == tz.zadatakId).Single());
            }
            ViewBag.Predmet = dohvatiNazivPredmeta(dummy);
            Tuple<Test, IEnumerable<Zadatak>> tuple = new Tuple<Test, IEnumerable<Zadatak>>(dummy, zadaci);
            return View(tuple);
        }

        // GET: Tests/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int? predmet)
        {
            if (!Request.IsAuthenticated)   // onemoguci pristup neregistriranim korisnicima
            {
                return RedirectToAction("Index", "Home");
            }
            Test test = new Test();
            test.brojPitanja = 10;  // default postavljanje na 10
            try
            {
                test.predmetId = (int)predmet;
            }
            catch (InvalidOperationException ioe)
            {
                ;   // ovdje napraviti kaj bude se radilo ako nije zadan predmet
            }
            return View(test);
        }

        // POST: Tests/Create
        public ActionResult Create([Bind(Include = "testId,userId,brojPitanja")] Test test, int? predmet)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            test.brojPitanja = 10;
            try
            {
                test.predmetId = (int)predmet;
            }
            catch (InvalidOperationException ioe)
            {
                ;   // ovdje napraviti kaj bude se radilo ako nije zadan predmet
            }
            return View(test);
        }

        // GET: Tests/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Test test = db.Tests.Find(id);
            if (test == null)
            {
                return HttpNotFound();
            }
            return View(test);
        }

        // POST: Tests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "testId,userId,brojPitanja")] Test test)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (ModelState.IsValid)
            {
                db.Entry(test).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(test);
        }

        // GET: Tests/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Test test = db.Tests.Find(id);
            if (test == null)
            {
                return HttpNotFound();
            }
            return View(test);
        }

        // POST: Tests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            Test test = db.Tests.Find(id);
            db.Tests.Remove(test);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Leaderboard(int predmet = 0, int bracket = 0)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            List<Test> tests = poredakTestova(predmet, bracket);

            foreach (Test t in tests)
            {
                if(db.Users.Where(c => c.Id.Equals(t.user)).FirstOrDefault() != null)
                {
                    t.username = db.Users.Where(c => c.Id.Equals(t.user)).First().UserName;
                }
            }
            List<String> subjects = new List<string>();
            foreach (Test t in tests)
            {
                if (db.Users.Where(c => c.Id.Equals(t.user)).FirstOrDefault() != null)
                {
                    subjects.Add(dohvatiNazivPredmeta(t));
                }               
            }
            Tuple<IEnumerable<Test>, IEnumerable<string>, int, int> tuple = new Tuple<IEnumerable<Test>, IEnumerable<string>, int, int>(tests.Take(brojPrikazanih), subjects, predmet, bracket);
            return View(tuple);
        }

        [HttpPost]
        public ActionResult Start(int? predmet,int? brojPitanja)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (predmet == null)    // predmet nije zadan, javi error
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Zadatak> zadaci = new List<Zadatak>();
            List<Zadatak> povratni = new List<Zadatak>();
            zadaci = db.zadaci.Include(z => z.cjelina).Include(z => z.statistikaDM).Where(z => z.cjelina.predmetId==predmet).ToList();
            int max = zadaci.Count; // broj zadataka u skupini
            int pitanja = konstBrojPitanja;
            if (brojPitanja != null)
            {
                pitanja = (int)brojPitanja;
            }
            int duplicatePitanja = pitanja;
            List<Int32> ids = new List<int>();
            List<String> napomene = new List<string>();
            while (pitanja > 0)
            {
                Zadatak zadatak = zadaci[getNextId(ids,max)];
                if (zadatak == null)
                {
                    return HttpNotFound();
                }
                povratni.Add(zadatak);
                pitanja--;
            }

            Session["Rem_Time"] = DateTime.Now.AddMinutes(duplicatePitanja*3).ToUniversalTime().ToString("dd-MM-yyyy H:mm:ss");
            ViewBag.Message = "Test je gotov.";
            ViewBag.EndDate = Session["Rem_Time"];

            test.brojPitanja = duplicatePitanja;
            test.testZadaci = generateTestZadatak(povratni);
            napomene = generateNapomene(povratni);
            test.user = User.Identity.GetUserId();
            test.datum = DateTime.Now;
            test.vrijeme = 0;
            test.bodovi = 0;
            Tuple<Test, IEnumerable<string>> tuple;
            tuple = new Tuple<Test, IEnumerable<string>>(test, napomene);
            return View(tuple);
        }

        private List<string> generateNapomene(List<Zadatak> povratni)
        {
            List<string> napomene = new List<string>();
            foreach (Zadatak zad in povratni)
            {
                napomene.Add(zad.napomena);
            }
            return napomene;
        }

        private List<TestZadatak> generateTestZadatak(List<Zadatak> zadaci)
        {
            List<TestZadatak> output = new List<TestZadatak>();
            foreach (Zadatak zad in zadaci)
            {
                TestZadatak z = new TestZadatak();
                z.test = test;
                z.testId = test.testId;
                z.zadatak = zad;
                z.zadatakId = zad.zadatakId;
                if (zad.tipZadatka == 1)    // ako je zadatak na zaokruzivanje
                {
                    z = shuffle(z, zad);
                }
                else if (zad.tipZadatka == 3)   // zadaci sa zadanom slikom s odg. se ne shuffleaju
                {
                    z.odgA = zad.tekstARjesenje; z.odgB = zad.tekstBRjesenje; z.odgC = zad.tesktCRjesenje; z.odgD = zad.tesktDRjesenje; z.tocnoRj = zad.tocnoRjesenje;
                }
                else
                {        // za ostale tipove, tocno rjesenje postaje zapis tamo
                    z.tocnoRj = zad.tocnoRjesenje;
                }
                z.tocnost = false;  // postavljamo na tocno na pocetku
                output.Add(z);
            }
            return output;
        }

        private TestZadatak shuffle(TestZadatak z, Zadatak zad)
        {
            Dictionary<string, int> noviMapaOdgovor = new Dictionary<string, int>()
            {
                {"A", 0 },
                { "B", 1 },
                { "C", 2 },
                { "D", 3 }
            };
            List<String> tekstoviRj = new List<String>(new String[] { zad.tekstARjesenje, zad.tekstBRjesenje, zad.tesktCRjesenje, zad.tesktDRjesenje });
            List<Int32> ids = new List<int>();
            int indTocno = noviMapaOdgovor[zad.tocnoRjesenje];  // dobijamo index tocnog rjesenja
            int i = 0;
            foreach (String text in tekstoviRj)
            {
                int rj = getNextId(ids, 4);
                if (rj == indTocno)    // ovdje pohranjujemo u tocno rjesenje slovo ispred tocnog odg
                    z.tocnoRj = noviMapaOdgovor.FirstOrDefault(x => x.Value == i).Key;
                switch (i)
                {
                    case 0: z.odgA = tekstoviRj[rj]; break;
                    case 1: z.odgB = tekstoviRj[rj]; break;
                    case 2: z.odgC = tekstoviRj[rj]; break;
                    case 3: z.odgD = tekstoviRj[rj]; break;
                }
                i++;
            }
            return z;
        }


        // POST: Tests/Finish/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Finish(Test test)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            String vrijeme = Session["Rem_Time"].ToString();

            Test zaBazu = new Test();
            Test pomocni = new Test();
            double postotak;
            int postotak2;

            zaBazu.user = test.user;
            zaBazu.brojPitanja = test.brojPitanja;
            zaBazu.vrijeme = test.brojPitanja * 3 * 60 - preostaloUTC(vrijeme.Split(' ')[1].Split(':'));
            test.vrijeme = zaBazu.vrijeme;
            zaBazu.datum = test.datum;
            pomocni = db.Tests.Add(zaBazu);
                db.SaveChanges();


            test.testId = pomocni.testId;               // promjena testId-a da odgovara onome u bazi
            foreach (TestZadatak tz in test.testZadaci)
            {
                tz.testId = test.testId;
            }
            test.testZadaci = spremiZadatke(test.testZadaci);
            test.tocnih = provjera(test);

            postotak = test.tocnih;
            postotak /= test.brojPitanja;
            postotak2 = (int)(postotak * 100 * test.tocnih);
            test.bodovi = postotak2;

            db.Tests.Find(test.testId).tocnih = provjera(test);
            db.Tests.Find(test.testId).bodovi = test.bodovi;
            db.SaveChanges();

            return View(test);
        }

        /// <summary>
        /// metoda koja provjerava tocnost odgovora korisnika, osvjezava statistiku u bazi i vraca
        /// broj tocnih odgovora
        /// </summary>
        /// <param name="test">objekt test</param>
        /// <returns>broj tocnih odgovora na testu</returns>
        private int provjera(Test test)
        {
            int tocnih = 0;
            foreach (TestZadatak tz in test.testZadaci)
            {
                Zadatak zad = new Zadatak();
                TestZadatak pom = new TestZadatak();
                pom = db.TestZadataks.Find(tz.testZadatakId);
                zad = db.zadaci.Find(pom.zadatakId);
                if (zad.tipZadatka == 2 || zad.tipZadatka == 5)
                {
                    try
                    {
                        if (pom.rjesenje.Equals(pom.tocnoRj))
                        {   // provjeravamo ako je unos doslovno tocan jer iz nekog razloga se tocni zeznu
                            pom.tocnost = true;
                            zad.brTocnih++;
                            tocnih++;
                            continue;
                        }
                    } catch (Exception ei) {; }
                    pom.rjesenje = preoblikuj(pom.rjesenje);
                    if (usporediPogresku(pom.rjesenje, pom.tocnoRj))
                    {   // pogreska je dovoljno malena, tocno je!
                        pom.tocnost = true;
                        zad.brTocnih++;
                        tocnih++;
                        continue;
                    }
                }
                else if(zad.tipZadatka == 1)    // STATISTIKA ZAOKRUZIVANJA
                {   // tipovi zadatka na zaokruživanje
                    string odg = "";
                    switch (pom.rjesenje)
                    {
                        case "A": odg = pom.odgA; break;
                        case "B": odg = pom.odgB; break;
                        case "C": odg = pom.odgC; break;
                        case "D": odg = pom.odgD; break;
                    }
                    if (odg.Equals(zad.tekstARjesenje)) { zad.brOdgA++; }
                    else if (odg.Equals(zad.tekstBRjesenje)) { zad.brOdgB++; }
                    else if (odg.Equals(zad.tesktCRjesenje)) { zad.brOdgC++; }
                    else if (odg.Equals(zad.tesktDRjesenje)) { zad.brOdgD++; }
                }
                else
                {
                    switch (pom.rjesenje)
                    {
                        case "A": zad.brOdgA++; break;
                        case "B": zad.brOdgB++; break;
                        case "C": zad.brOdgC++; break;
                        case "D": zad.brOdgD++; break;
                    }

                }
                if (pom.rjesenje == null || !pom.rjesenje.Equals(pom.tocnoRj))  // TOCNOST ZAOKRUZIVANJA
                {
                    zad.brNetocnih++;
                }
                else
                {
                    pom.tocnost = true;
                    zad.brTocnih++;
                    tocnih++;
                }
            }
            db.SaveChanges();
            return tocnih;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testZadaci"></param>
        /// <returns></returns>
        private List<TestZadatak> spremiZadatke(List<TestZadatak> testZadaci)
        {
            foreach (TestZadatak zadatak in testZadaci)
            {
                TestZadatak pomocni = new TestZadatak();
                pomocni = db.TestZadataks.Add(zadatak);
                zadatak.testZadatakId = pomocni.testZadatakId;
            }
            db.SaveChanges();
            return testZadaci;
        }

        /// <summary>
        /// metoda koja preoblikuje string u odgovor slican tocnom
        /// </summary>
        /// <param name="odgovor">ulazni string od korisnika</param>
        /// <returns>preoblikovan u prihvatljiv format</returns>
        public string preoblikuj(string odgovor)
        {
            try
            {
                odgovor = odreziNule(odgovor);
                odgovor = odgovor.Trim();  // podrezivanje pocetka i kraja
            }
            catch (Exception e){
                return odgovor;
            }
            if (odgovor.Contains("."))
            {  // zamjena tocki s zarezima
                odgovor = odgovor.Replace('.', ',');
            }
            odgovor = odgovor.ToLower();
            odgovor = odgovor.Replace("="," = ");   //zamjeni jednako s razmak-jednako-razmak,
            odgovor = odgovor.Replace("\\s+"," ");  // vise*razmak => 1*razmak (BITAN POREDAK, nakon " = ")
            odgovor = odgovor.Replace(" ,", ",");   // micanje razmaka ispred odgovora
            odgovor = Regex.Replace(odgovor,",([a-z])", ", $1");    // stavlja razmak ispred slova
            odgovor = odgovor.Replace("\\s+", " "); // ZADNJA PROMJENA
            return odgovor;
        }

        public string odreziNule(string odgovor)
        {   // MICANJE 0 S POCETKA
            odgovor = odgovor.TrimStart(new Char[] { '0' });    // micanje pocetnih nula
            if (odgovor.First().Equals('.') || odgovor.First().Equals(','))
            {  // dodavanje 0 ispred ,/. ako je makne prethodna linija
                odgovor = "0" + odgovor;
            }
            if (odgovor == null) { return "0"; }    // ako se maknu sve nule, znaci da je odgovor bio nula
            return odgovor;
        }
        
        public Boolean usporediPogresku(string odg, string tocno)
        {
            Double odg1, toc;
            try
            {
                System.Diagnostics.Debug.WriteLine(odg + " RAZMAK " + tocno);
                if (odg.Equals(tocno))
                    return true;    // provjerava one zadatke gdje je odgovor s zarezima
                odg1 = Double.Parse(odg.Replace(",", "."));
                toc = Double.Parse(tocno.Replace(",", "."));
            } catch(Exception e)
            {
                    return false;   // imas neke greske
            }
            if (Math.Abs(odg1 - toc) < 0.1*Math.Abs(toc))
                return true;
            else
                return false;
        }

        /// <summary>
        /// metoda racuna preostalo vrijeme koristeci UTC za laksu sinkronizaciju
        /// </summary>
        /// <param name="hms">zapisi sati, minuta i sekundi iz vremena</param>
        /// <returns>preostalo vrijeme</returns>
        public Int32 preostaloUTC(String[] hms)
        {
            System.DateTime universal = DateTime.Now.ToUniversalTime();
            Int32 h = Int32.Parse(hms[0]);
            Int32 m = Int32.Parse(hms[1]);
            Int32 s = Int32.Parse(hms[2]);
            int ocekivano = h * 3600 + m * 60 + s;
            int trenutno = universal.Hour * 3600 + universal.Minute * 60 + universal.Second;
            return ocekivano - trenutno;
        }

        /// <summary>
        /// metoda generira svaki put drugi random broj
        /// </summary>
        /// <param name="alreadyGenerated">vec izgenerirani brojevi</param>
        /// <param name="max">najveci moguci broj</param>
        /// <returns>novo dodani broj u listu</returns>
        public int getNextId(List<Int32> alreadyGenerated,int max)
        {
            int broj = 0;
            do
            {
                broj = rnd.Next(max);
            } while (alreadyGenerated.Contains(broj));
            alreadyGenerated.Add(broj);
            return broj;
        }
        
        /// <summary>
        /// metoda koja u bazi dohvaca naziv predmeta iz kojeg je test pisan
        /// </summary>
        /// <param name="t">referenca na objekt test</param>
        /// <returns>string koji predstavlja naziv predmeta</returns>
        private string dohvatiNazivPredmeta(Test t)
        {
            return db.TestZadataks.Where(b => b.testId == t.testId).ToList().FirstOrDefault().zadatak.cjelina.predmet.naziv;
        }

        /// <summary>
        /// metoda koja sluzi za promjenu poretka testova
        /// </summary>
        /// <param name="predmet">id predmeta poretka</param>
        /// <param name="bracket">podskup poretka, nacelno po broju pitanja</param>
        /// <returns>poredak ispita na leaderboardu poredan na zeljeni nacin</returns>
        private List<Test> poredakTestova(int predmet, int bracket)
        {
            if (predmet == 0 && bracket == 0)
            {
                return db.Tests.OrderByDescending(b => b.bodovi).ThenByDescending(b => b.tocnih).ThenBy(b => b.vrijeme).ToList();
            }
            else if (predmet == 1 && bracket == 0)
            {
                return db.Tests.Where(b => db.TestZadataks.Where(c => c.testId == b.testId).ToList().FirstOrDefault().zadatak.cjelina.predmet.naziv == "Matematika")
                    .OrderByDescending(c => c.bodovi).ThenByDescending(c => c.tocnih).ThenBy(c => c.vrijeme).ToList();
            }
            else if (predmet == 2 && bracket == 0)
            {
                return db.Tests.Where(b => db.TestZadataks.Where(c => c.testId == b.testId).ToList().FirstOrDefault().zadatak.cjelina.predmet.naziv == "Fizika")
                    .OrderByDescending(c => c.bodovi).ThenByDescending(c => c.tocnih).ThenBy(c => c.vrijeme).ToList();
            }
            else if (predmet == 3 && bracket == 0)
            {
                return db.Tests.Where(b => db.TestZadataks.Where(c => c.testId == b.testId).ToList().FirstOrDefault().zadatak.cjelina.predmet.naziv == "Programiranje")
                    .OrderByDescending(c => c.bodovi).ThenByDescending(c => c.tocnih).ThenBy(c => c.vrijeme).ToList();
            }
            // Svi predmeti
            else if (predmet == 0 && bracket == 1)
            {
                return db.Tests.Where(b => b.brojPitanja < 10)
                    .OrderByDescending(b => b.bodovi).ThenByDescending(b => b.tocnih).ThenBy(b => b.vrijeme).ToList();
            }
            else if (predmet == 0 && bracket == 2)
            {
                return db.Tests.Where(b => b.brojPitanja >= 10 && b.brojPitanja <= 20)
                    .OrderByDescending(b => b.bodovi).ThenByDescending(b => b.tocnih).ThenBy(b => b.vrijeme).ToList();
            }
            else if (predmet == 0 && bracket == 3)
            {
                return db.Tests.Where(b => b.brojPitanja > 20)
                    .OrderByDescending(b => b.bodovi).ThenByDescending(b => b.tocnih).ThenBy(b => b.vrijeme).ToList();
            }
            // Matematika
            else if (predmet == 1 && bracket == 1)
            {
                return db.Tests.Where(b => b.brojPitanja < 10).Where(b => db.TestZadataks.Where(c => c.testId == b.testId).ToList().FirstOrDefault().zadatak.cjelina.predmet.naziv == "Matematika")
                    .OrderByDescending(b => b.bodovi).ThenByDescending(b => b.tocnih).ThenBy(b => b.vrijeme).ToList();
            }
            else if (predmet == 1 && bracket == 2)
            {
                return db.Tests.Where(b => b.brojPitanja >= 10 && b.brojPitanja <= 20).Where(b => db.TestZadataks.Where(c => c.testId == b.testId).ToList().FirstOrDefault().zadatak.cjelina.predmet.naziv == "Matematika")
                    .OrderByDescending(b => b.bodovi).ThenByDescending(b => b.tocnih).ThenBy(b => b.vrijeme).ToList();
            }
            else if (predmet == 1 && bracket == 3)
            {
                return db.Tests.Where(b => b.brojPitanja > 20).Where(b => db.TestZadataks.Where(c => c.testId == b.testId).ToList().FirstOrDefault().zadatak.cjelina.predmet.naziv == "Matematika")
                    .OrderByDescending(b => b.bodovi).ThenByDescending(b => b.tocnih).ThenBy(b => b.vrijeme).ToList();
            }
            // Fizika
            else if (predmet == 2 && bracket == 1)
            {
                return db.Tests.Where(b => b.brojPitanja < 10).Where(b => db.TestZadataks.Where(c => c.testId == b.testId).ToList().FirstOrDefault().zadatak.cjelina.predmet.naziv == "Fizika")
                    .OrderByDescending(b => b.bodovi).ThenByDescending(b => b.tocnih).ThenBy(b => b.vrijeme).ToList();
            }
            else if (predmet == 2 && bracket == 2)
            {
                return db.Tests.Where(b => b.brojPitanja >= 10 && b.brojPitanja <= 20).Where(b => db.TestZadataks.Where(c => c.testId == b.testId).ToList().FirstOrDefault().zadatak.cjelina.predmet.naziv == "Fizika")
                    .OrderByDescending(b => b.bodovi).ThenByDescending(b => b.tocnih).ThenBy(b => b.vrijeme).ToList();
            }
            else if (predmet == 2 && bracket == 3)
            {
                return db.Tests.Where(b => b.brojPitanja > 20).Where(b => db.TestZadataks.Where(c => c.testId == b.testId).ToList().FirstOrDefault().zadatak.cjelina.predmet.naziv == "Fizika")
                    .OrderByDescending(b => b.bodovi).ThenByDescending(b => b.tocnih).ThenBy(b => b.vrijeme).ToList();
            }
            // Informatika
            else if (predmet == 3 && bracket == 1)
            {
                return db.Tests.Where(b => b.brojPitanja < 10).Where(b => db.TestZadataks.Where(c => c.testId == b.testId).ToList().FirstOrDefault().zadatak.cjelina.predmet.naziv == "Programiranje")
                    .OrderByDescending(b => b.bodovi).ThenByDescending(b => b.tocnih).ThenBy(b => b.vrijeme).ToList();
            }
            else if (predmet == 3 && bracket == 2)
            {
                return db.Tests.Where(b => b.brojPitanja >= 10 && b.brojPitanja <= 20).Where(b => db.TestZadataks.Where(c => c.testId == b.testId).ToList().FirstOrDefault().zadatak.cjelina.predmet.naziv == "Programiranje")
                    .OrderByDescending(b => b.bodovi).ThenByDescending(b => b.tocnih).ThenBy(b => b.vrijeme).ToList();
            }
            else if (predmet == 3 && bracket == 3)
            {
                return db.Tests.Where(b => b.brojPitanja > 20).Where(b => db.TestZadataks.Where(c => c.testId == b.testId).ToList().FirstOrDefault().zadatak.cjelina.predmet.naziv == "Programiranje")
                    .OrderByDescending(b => b.bodovi).ThenByDescending(b => b.tocnih).ThenBy(b => b.vrijeme).ToList();
            }
            return null;
        }
    }
    
}
