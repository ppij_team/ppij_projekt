﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PostaniFerovac2.Models;

namespace PostaniFerovac2.Controllers
{
    public class TestController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Test
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Start(int? brojPitanja)
        {
            string tip = "na";
            if (tip.Equals("Fizika"))
            {
                // ocitavaj iz fizikalnih
            }
            else if (tip.Equals("Matematika"))
            {
                // ocitavaj iz matematickih zadataka
            }
            int max = 4; // ovo ce poprimiti vrijednost broja zadataka u toj skupini, 4 placeholder
            List<Zadatak> zadaci = new List<Zadatak>();
            while (brojPitanja >= 0)
            {
                Zadatak zadatak = db.zadaci.Find(generateRandomId(max));
                if (zadatak == null)
                {
                    return HttpNotFound();
                }
                zadaci.Add(zadatak);
                brojPitanja--;
            }
            return View(zadaci);
        }

        private Int32 generateRandomId(int max)
        {
            Random rnd = new Random();
            return rnd.Next(max);
        }

        private ActionResult CountTime()
        {
            Stopwatch sw = new Stopwatch(); // mjerimo vrijeme po testu

            sw.Start();

            // ...

            int limit = 60;
            if (sw.Elapsed.Equals(limit))
            {
                sw.Stop();
                Console.WriteLine("Elapsed={0}", sw.Elapsed);
                return View();
            }
            return null;

        }
    }
}